﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sessão_da_tarde.Models;
using System;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using Sessão_da_tarde.Services;
using Sessão_da_tarde.Repositories;
using Sessão_da_tarde.Interface;

namespace Sessão_da_tarde.Controllers
{
    [Route("v1/account")]
    public class HomeController : Controller
    {
        private readonly IUserRepository _userRepository;
        
        public HomeController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody] usuario model)
        {
            // Recupera o usuário
            var user = _userRepository.Get(model.Email, model.Senha);

            // Verifica se o usuário existe
            if (user == null)
                return NotFound(new { message = "Usuário ou senha inválidos" });

            // Gera o Token
            var token = TokenService.GenerateToken(user);

            // Oculta a senha
            user.Senha = "";

            // Retorna os dados
            return new
            {
                user = user,
                token = token
            };
        }

        [HttpGet]
        [Route("anonymous")]
        [AllowAnonymous]
        public string Anonymous() => "Anônimo";

        [HttpGet]
        [Route("authenticated")]
        [Authorize]
        public string Authenticated() => String.Format("Autenticado - {0}", User.Identity.Name);

        [HttpGet]
        [Route("employee")]
        [Authorize(Roles = "employee,manager")]
        public string Employee() => "Funcionário logado no sistema";

        [HttpGet]
        [Route("manager")]
        [Authorize(Roles = "manager")]
        public string Manager() => "Gerente logado no sistema";
    }
}
