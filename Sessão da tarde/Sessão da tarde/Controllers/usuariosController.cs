﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sessão_da_tarde.Models;

namespace Sessão_da_tarde.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class usuariosController : ControllerBase
    {
        private readonly cinemaContext _context;

        public usuariosController(cinemaContext context)
        {
            _context = context;
        }

        // GET: api/usuarios
        [HttpGet]
        [Authorize(Roles = "employee,manager")]
        public async Task<ActionResult<IEnumerable<usuario>>> Getusuario()
        {
            return await _context.usuario.ToListAsync();
        }

        // GET: api/usuarios/5
        [HttpGet("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<ActionResult<usuario>> Getusuario(int id)
        {
            var usuario = await _context.usuario.FindAsync(id);

            if (usuario == null)
            {
                return NotFound();
            }

            return usuario;
        }

        // PUT: api/usuarios/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Putusuario(int id, usuario usuario)
        {
            if (id != usuario.id)
            {
                return BadRequest();
            }

            _context.Entry(usuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!usuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/usuarios
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "employee,manager")]
        public async Task<ActionResult<usuario>> Postusuario(usuario usuario)
        {
            _context.usuario.Add(usuario);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getusuario", new { id = usuario.id }, usuario);
        }

        // DELETE: api/usuarios/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<ActionResult<usuario>> Deleteusuario(int id)
        {
            var usuario = await _context.usuario.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            _context.usuario.Remove(usuario);
            await _context.SaveChangesAsync();

            return usuario;
        }

        private bool usuarioExists(int id)
        {
            return _context.usuario.Any(e => e.id == id);
        }
    }
}
