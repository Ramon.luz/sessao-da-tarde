﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Sessão_da_tarde.Models;

namespace Sessão_da_tarde.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class filmesController : ControllerBase
    {
        private readonly cinemaContext _context;

        public filmesController(cinemaContext context)
        {
            _context = context;
        }

        // GET: api/filmes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<filme>>> Getfilmes()
        {
            return await _context.filmes.ToListAsync();
        }

        [HttpGet]
        [Route("popularfilmes")]
        public async Task<ActionResult<IEnumerable<filmesExternosViewModel>>> GetfilmesPopular()
        {
            using var client = new HttpClient();
 
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzY2IzZDAxMjg0M2JmYzU0YWVlMGJjOTI1MWQ1MGQzYiIsInN1YiI6IjVmYmJmMzkzYWE2NTllMDAzZDk4YjRhNCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.rYF5nr1__ddyb2yA1rcnZ9kzH4PgM3qPsejrCuql76s");
            var result = await client.GetStringAsync("https://api.themoviedb.org/3/movie/popular?api_key=3cb3d012843bfc54aee0bc9251d50d3b&language=pt-BR");
            var content = JsonConvert.DeserializeObject<FilmesListViewModel>(result);
            var list = new List<dynamic>();
            return content.results.ToList();
        }
        // GET: api/filmes/5
        [HttpGet("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<ActionResult<filme>> Getfilme(int id)
        {
            var filme = await _context.filmes.FindAsync(id);

            if (filme == null)
            {
                return NotFound();
            }

            return filme;
        }

        // PUT: api/filmes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<IActionResult> Putfilme(int id, filme filme)
        {
            if (id != filme.id)
            {
                return BadRequest();
            }

            _context.Entry(filme).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!filmeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/filmes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "employee,manager")]
        public async Task<ActionResult<filme>> Postfilme(filme filme)
        {
            _context.filmes.Add(filme);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getfilme", new { id = filme.id }, filme);
        }

        // DELETE: api/filmes/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "employee,manager")]
        public async Task<ActionResult<filme>> Deletefilme(int id)
        {
            var filme = await _context.filmes.FindAsync(id);
            if (filme == null)
            {
                return NotFound();
            }

            _context.filmes.Remove(filme);
            await _context.SaveChangesAsync();

            return filme;
        }

        private bool filmeExists(int id)
        {
            return _context.filmes.Any(e => e.id == id);
        }
    }
}
