﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Sessão_da_tarde.Models
{
    public class cinemaContext : DbContext
    {
        public cinemaContext(DbContextOptions<cinemaContext> options)
            : base(options)
        {
        }
       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<filme>().ToTable("filmes");
            modelBuilder.Entity<filme>(entity =>
            {
                entity.HasKey(e => e.id);
                entity.Property(e => e.Nome).IsRequired();
                entity.Property(e => e.Genero).IsRequired();
                entity.Property(e => e.em_cartaz).IsRequired();
                entity.Property(e => e.Ano).IsRequired();
            });
            modelBuilder.Entity<usuario>().ToTable("usuarios");
            modelBuilder.Entity<usuario>(entity =>
            {
                entity.HasKey(e => e.id);
                entity.Property(e => e.Nome).IsRequired();
                entity.Property(e => e.Email).IsRequired();
                entity.Property(e => e.Senha).IsRequired();
            });
        }

        public DbSet<usuario> usuario { get; set; }
        public DbSet<filme> filmes { get; set; }

    }
}
