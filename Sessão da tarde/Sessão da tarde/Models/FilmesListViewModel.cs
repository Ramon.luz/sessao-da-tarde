﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sessão_da_tarde.Models
{
    public class FilmesListViewModel
    {
        public int page { get; set; }
        public int total_pages { get; set; }
        public IEnumerable<filmesExternosViewModel> results { get; set; }
    }
}
