﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sessão_da_tarde.Models
{
    public class InicializaBD
    {
        public static void Initialize(cinemaContext context)
        {
            context.Database.EnsureCreated();
            
            if (context.filmes.Any())
            {
                return;   //O BD foi alimentado
            }
            var filmes = new filme[]
            {
              new filme{Nome="Os Vingadores", Genero="Aventura", em_cartaz=true,
                Ano= DateTime.Now},
            };
            foreach (filme p in filmes)
            {
                context.filmes.Add(p);
            }
            context.SaveChanges();
        }
    }
}
