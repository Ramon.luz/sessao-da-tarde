﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sessão_da_tarde.Models
{
    public class filmesExternosViewModel
    {
        public string title { get; set; }
        public string release_date { get; set; }
        public long popularity { get; set; }
        public string overview { get; set; }
        
        
    }
}
