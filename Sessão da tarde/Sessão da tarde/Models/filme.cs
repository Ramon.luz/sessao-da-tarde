﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sessão_da_tarde.Models
{
    public class filme
    {
        public int id { get; set; }
        public string Nome { get; set; }
        public string Genero { get; set; }
        public DateTime Ano { get; set; }
        public Boolean em_cartaz { get; set; }
    }
}
